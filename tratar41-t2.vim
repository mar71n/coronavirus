:/Table\s*2
:/^\D\+\s\+\d\+\s\+(\d\+)\s\+\d\+
:1,.-d
:/Grand total
:.+2,$d
:g/Territories/d
:g/European Region/d
:g/South-East Asia Region/d
:g/Eastern Mediterranean Region/d
:g/Region of the Americas/d
:g/African Region/d
:g/[^0-9)]$/join|g/[^0-9)]$/join|g/[^0-9)]$/join|g/[^0-9)]$/join
:%s/^\(\D\+\)\(\d\+\s\+\)(\(\d\+\))\s*\(\d\+\s\+\)(\(\d\+\))\s*\(\D\+\)\(\d\+\)/"\1", \2, \3, \4, \5, "\6", \7
:%s/^"[^A-Za-z]/"/
:%s/^\(\D\+\)\(\d\+\s\+\)(\(\d\+\))\s\+\(\d\+\s\+\)(\(\d\+\))/"\1", \2, \3, \4, \5, ,
:normal gg
:normal O
:normal idate, place, totconfirmed, totconfirmednew, totdeaths, totdeathsnew, transmissionclass, dayslastrep
:2,$s/^\(.*$\)/"2020\/03\/13",\1
:%s/\s"/"
:w! sitrep-NN-table2.csv
:q!
