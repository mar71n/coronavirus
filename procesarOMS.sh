#!/bin/bash
pdftotext -layout $1.pdf
sed -i "/Territories\|European Region\|South-East Asia Region\|Eastern Mediterranean Region\|Region[s]\? of the Americas\|African Region/d" $1.txt 
sed -i "/Reporting Country\|Territory\/Area\|Total confirmed\|deaths\|classificationi\|Days since\|reported case\|Americas/d" $1.txt 
sed -i "/Eastern Mediterranean\|Europe\|South-East Asia\|Western Pacific/d" $1.txt 
# sed -i "/^China\s\+[0-9]\+\s\+[0-9]\+\s\+[0-9]\+\s\+[0-9]\+/ s/^/BORRARHASTAACA\n/" $1.txt
sed -i "/^\s*South\s*Africa\s\+[0-9]\+\s\+[0-9]\+\s\+[0-9]\+\s\+[0-9]\+/ s/^/BORRARHASTAACA\n/" $1.txt
sed -i "1,/BORRARHASTAACA/d" $1.txt 
# sed -i "/Grand total/ s/$/\nBORRARDESDEACA/" $1.txt
sed -i "/See\s\+Annex\s\+1/ s/^/\nBORRARDESDEACA\n/" $1.txt
sed -i "/BORRARDESDEACA/,\$d" $1.txt
sed -i "s/[^[:print:]]//g" $1.txt
sed -i "s/\[.*\]//g" $1.txt
sed -i "s/†//g" $1.txt
sed -i "s/\([[:digit:]]\)\s\([[:digit:]]\)/\1\2/g" $1.txt
sed -i ":x ; /[^[:digit:]]$/ { N ; s/\n/ /g ; bx }" $1.txt
sed -i ":x ; /[[:digit:]]\+\s[[:digit:]]*$/ { N ; s/\n/ /g ; bx }" $1.txt
sed -i 's/^\([^[:digit:]]\+\)\([[:digit:]]\+\s\+\)\([-[:digit:]]\+\s\+\)\([[:digit:]]\+\s\+\)\([-[:digit:]]\+\s\+\)\([^[:digit:]]\+\)\([[:digit:]]\+\)/"\1", \2, \3, \4, \5, "\6", \7/g' $1.txt
sed -i 's/^\([^[:digit:]]\+\)\([[:digit:]]\+\s\+\)\([-[:digit:]]\+\s\+\)\([[:digit:]]\+\s\+\)\([-[:digit:]]\+\s\+\)/"\1", \2, \3, \4, \5, ,/g' $1.txt
sed -i '/^$/d' $1.txt
sed -i 's/\s\+"/"/g' $1.txt
sed -i 's/"\s\+/"/g' $1.txt
sed -i 's/\s\+,/,/g' $1.txt
if true; then
sed -i 's/^\(.*$\)/"2020\/03\/13",\1/' $1.txt
sed -i "1 s/^/date, place, totconfirmed, totconfirmednew, totdeaths, totdeathsnew, transmissionclass, dayslastrep\n/" $1.txt
fi
cp $1.txt sitrep-NN-table2.csv
vi sitrep-NN-table2.csv
