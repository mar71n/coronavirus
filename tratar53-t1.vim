:/Table\s*1
:/^\D\+\s\+\d\+\s\+\d\+\s\+\d\+
:1,.-d
:/Total
:.+1,$d
:%s/^\(\D\+\s\+\)\(\d\+\s\+\)\(\d\+\s\+\)\(\d\+\s\+\)\(\d\+\s\+\)\(\d\+\s\+\)\(\d\+\)/"\1",\2,\3,\4,\5,\6,\7
:normal gg
:normal O
:normal iplace, pop10k, conf24h, susp24h, deaths24h, confirmedagg, deathsagg  
:%s/\s"/"
:2,$s/^\(.*$\)/"2020\/03\/13",\1
:normal gg
:normal idate, 
:w! sitrep-NN-table1.csv
:q!
